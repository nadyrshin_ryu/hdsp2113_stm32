//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <stdlib.h>
#include <string.h>
#include <stm32f10x.h>
#include <stm32f10x_rcc.h>
#include <stm32f10x_gpio.h>
#include <delay.h>
#include <hdsp2113.h>
#include "main.h"


char TestStr[] = "����������� � ���������";

void main()
{
  uint32_t Cntr = 0;
  
  SystemInit();

  hdsp_init();

  while (1)
  {
    // ����������� �������
    for (uint8_t i = 0; i < 8; i++)
    {
      hdsp_SetBrightness(i);
      hdsp_printf(0, "���. %d", i);
      delay_ms(1000);
    }

    hdsp_Clear();
    delay_ms(500);
    
    // ������� ������ � �������
    // ��������
    for (uint8_t i = 0; i < 7; i++)
    {
      hdsp_puts(7 - i, TestStr);
      delay_ms(250);
    }
    // ���������
    for (uint8_t i = 0; i < sizeof(TestStr) - 8; i++)
    {
      char TestSubstr[9];
      memcpy(TestSubstr, TestStr + i, 8);
      hdsp_puts(0, TestSubstr);
      delay_ms(250);
    }
    // �������
    for (uint8_t i = 0; i < 7; i++)
    {
      char TestSubstr[9];
      memcpy(TestSubstr, TestStr + sizeof(TestStr) - 8 + i, 7 - i);
      TestSubstr[7 - i] = ' ';
      TestSubstr[8 - i] = 0;
      hdsp_puts(0, TestSubstr);
      delay_ms(250);
    }
    
    hdsp_Clear();
    delay_ms(500);
   
    // ������ ���������� (����)
    for (uint8_t i = 0, Cntr = 0; i < 15; i++, Cntr++)
    {
      if (i % 2)
        hdsp_printf(0, "%02d:%02d:%02d", 12, 34, Cntr);
      else
        hdsp_printf(0, "%02d %02d %02d", 12, 34, Cntr);
      
      delay_ms(1000);
    }    
    
    hdsp_Clear();
    delay_ms(500);
    
    // ������������ ���������������� ������
    for (uint8_t i = 0, Cntr = 0; i < 150; i++, Cntr++)
    {
      hdsp_printf(0, "���! %d", Cntr);
      delay_ms(100);
    }
    
    hdsp_Clear();
    delay_ms(500);
  }
}
