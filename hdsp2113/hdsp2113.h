//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef _HDSP2113_H
#define _HDSP2113_H

#include <stdint.h>


#define HDSP_COLS               8       // ���-�� ��������� ����������

// ���� ����, ��� ������������ ���������� ����������������� ������ RESET ����������
#define HDSP_Reset_PinUsed      1       // ���� ���� ����, �� ����� ����������� ���������� �� ��������.
                                        // ����� Reset ����� ��������� � ���. 1
// ���� ����, ��� ������������ ���������� ����������������� ������ RD ����������
#define HDSP_RD_PinUsed         0       // ���� ���� ����, �� ������ �� ����������� ���������� �� ��������. 
                                        // ����� RD ����� ��������� � ���. 1
// ���� ����, ��� ������������ ���������� ����������������� ������ WR ����������
#define HDSP_WR_PinUsed         0       // ���� ���� ����, �� ���������� ������� �� ������. 
                                        // ����� WR � ���� ������ ����� ��������� � ���. 0.
// ���� ����, ��� ������������ ���������� ����������������� ������ FL ����������
#define HDSP_FL_PinUsed         0       // ���� ���� ����, �� ������� ������� �������� �� ��������. 
                                        // ����� FL ����� ��������� � ���. 1


//// ���� ����������������, ����������� ����������� hdsp2113
// ������ RESET
#if HDSP_Reset_PinUsed
#define HDSP_Reset_Port         GPIOB
#define HDSP_Reset_Pin          GPIO_Pin_15
#endif
// ������ FL (FLASH)
#if HDSP_FL_PinUsed
#define HDSP_FL_Port            GPIOA
#define HDSP_FL_Pin             GPIO_Pin_15
#endif
// ������ RD (READ)
#if HDSP_RD_PinUsed
#define HDSP_RD_Port            GPIOA
#define HDSP_RD_Pin             GPIO_Pin_5
#endif
// ������ WR (WRITE)
#if HDSP_WR_PinUsed
#define HDSP_WR_Port            GPIOB
#define HDSP_WR_Pin             GPIO_Pin_5
#endif
// ������ CE (CHIP ENABLE)
#define HDSP_CE_Port            GPIOC
#define HDSP_CE_Pin             GPIO_Pin_15
// ���� ���� ������ (���� A0-A4) ������������� ���������� HDSP
#define HDSP_Addr_Port          GPIOA
#define HDSP_Addr_Shift         8      // ����� ��� ������ � ����� GPIO (���� 0 - ������������ ������� 5 ��� �����)
// ���� ���� ������ (���� D0-D7) ������������� ���������� HDSP
#define HDSP_Data_Port          GPIOA
#define HDSP_Data_Shift         0      // ����� ��� ������ � ����� GPIO (���� 0 - ������������ ������� 8 ��� �����). ��������� ������ �� STM32, �.�. � AVR ���� ������ �������� ���� �������



// ��������� ������������� �������
void hdsp_init(void);
// ��������� ���������� ����� ����������� ���������� � �������� ������ �����
void hdsp_Reset(void);
// ��������� ������� ���������
void hdsp_Clear(void);
// ��������� ��������� ���� ����������
void hdsp_SelfTest(void);
// ��������� ��������/��������� ������� ���� ���������
void hdsp_Blink(uint8_t OnOff);
// ��������� ������������� ������� ���������� (0-7)
void hdsp_SetBrightness(uint8_t Value);
// ��������� �������� ANSI-������ � �������� ������� ����������
// StartIdx - ��������� ���������� ��� ������
void hdsp_puts(uint8_t StartIdx, char *str);
// ��������� ���������������� ������ ������� � �������� �������
void hdsp_printf(uint8_t StartIdx, const char *args, ...);

#if HDSP_FL_PinUsed
// ��������� ��������/��������� ������� ������� �� �������
void hdsp_FlashChar_OnOff(uint8_t CharNum, uint8_t OnOff);
#endif


#endif